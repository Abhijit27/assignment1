package com.pricing.marketmaker;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Objects;
import java.util.logging.Logger;

import com.pricing.connection.QuoteConnectionHandler;

public class PricingServerClient {
	String buyRequest = "1 BUY 20";
	String sellRequest = "4 SELL 60";
	private final int port = 8888;
	InetSocketAddress hostAddress = new InetSocketAddress(this.port);

	Logger logger = Logger.getLogger(QuoteConnectionHandler.class.getSimpleName());

	public void test_BuyRequest() {
		try (SocketChannel client = SocketChannel.open(this.hostAddress)) {
			logger.info("Sending Request: " + buyRequest);
			final ByteBuffer buffer = ByteBuffer.wrap((buyRequest).trim().getBytes());

			writeIntoChannel(buffer, client);

			ByteBuffer bufferResponse = ByteBuffer.allocate(15);
			client.read(bufferResponse);
			final String price = new String(bufferResponse.array()).trim();

			logger.info(String.format("Quote Response for :%s \t | Price : %s", buyRequest, price.toString()));
		} catch (IOException e) {
			throw new RuntimeException("Server connection/channel error.", e);
		}
	}
	
	public void test_SellRequest() {
		try (SocketChannel client = SocketChannel.open(this.hostAddress)) {
			logger.info("Sending Request: " + sellRequest);
			final ByteBuffer buffer = ByteBuffer.wrap((sellRequest).trim().getBytes());

			writeIntoChannel(buffer, client);

			ByteBuffer bufferResponse = ByteBuffer.allocate(15);
			client.read(bufferResponse);
			final String price = new String(bufferResponse.array()).trim();

			logger.info(String.format("Quote Response for :%s \t | Price : %s", sellRequest, price.toString()));
		} catch (IOException e) {
			throw new RuntimeException("Server connection/channel error.", e);
		}
	}

	void writeIntoChannel(final ByteBuffer buffer, final SocketChannel channel) throws IOException {
		if (Objects.isNull(buffer) || Objects.isNull(channel)) {
			throw new IllegalArgumentException("Error in writing in channel");
		}

		while (buffer.hasRemaining()) {
			channel.write(buffer);
		}
	}
	
	public static void main(String args[]) {
		
		while(true) {
			new PricingServerClient().test_BuyRequest();
			new PricingServerClient().test_SellRequest();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}

}
