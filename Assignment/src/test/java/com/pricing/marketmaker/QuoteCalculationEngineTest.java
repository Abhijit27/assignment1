package com.pricing.marketmaker;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class QuoteCalculationEngineTest {

    private QuoteCalculationEngine quoteCalculationEngine = new QuoteCalculationEngineImpl();

    @Test
    public void quoteCalculationTest() throws Exception{
        double quote = quoteCalculationEngine.computeQuote(1, 1, 100, 5);
        assertEquals(quote, 2.5, 0.0000001);
    }
}
