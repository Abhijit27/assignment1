package com.pricing.connection;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.logging.Logger;

/**
 * This client class is to test with server connectivity and quote response
 */
public final class QuoteClient implements SocketChannelWriter {

    Logger logger = Logger.getLogger(QuoteClient.class.getSimpleName());

    private final InetSocketAddress hostAddress;

    public static void main(final String[] args) {

        Thread t1 = new Thread(() -> {
            new QuoteClient(Integer.valueOf(args[0])).start("1 BUY 100");

        });

        Thread t2 = new Thread(() -> {
            new QuoteClient(Integer.valueOf(args[0])).start("2 SELL 200");

        });
        Thread t3 = new Thread(() -> {
            new QuoteClient(Integer.valueOf(args[0])).start("3 BUY 300");
        });
        Thread t4 = new Thread(() -> {
            new QuoteClient(Integer.valueOf(args[0])).start("9 SELL 900");

        });

        t1.start();
        t2.start();
        t3.start();
        t4.start();

        new QuoteClient(Integer.valueOf(args[0])).start("5 BUY 100");
        new QuoteClient(Integer.valueOf(args[0])).start("6 BUY 100");
        new QuoteClient(Integer.valueOf(args[0])).start("7 SELL 300");
        new QuoteClient(Integer.valueOf(args[0])).start("8 BUY 500");

    }


    private QuoteClient(final int port) {
        this.hostAddress = new InetSocketAddress(port);
    }

    private void start(final String message) {

        try (SocketChannel client = SocketChannel.open(this.hostAddress)) {
            logger.info("writing: " + message);
            final ByteBuffer buffer = ByteBuffer.wrap((message).trim().getBytes());

            writeIntoChannel(buffer, client);

            ByteBuffer bufferResponse = ByteBuffer.allocate(15);
            client.read(bufferResponse);
            final String price = new String(bufferResponse.array()).trim();

            logger.info(String.format("Quote Response for :%s \t | Price : %s", message, price.toString()));
        } catch (IOException e) {
            throw new RuntimeException("Server connection/channel error.", e);
        }
    }

}

