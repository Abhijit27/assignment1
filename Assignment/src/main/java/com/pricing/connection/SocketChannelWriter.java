package com.pricing.connection;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Objects;

public interface SocketChannelWriter {

    default void writeIntoChannel(final ByteBuffer buffer, final SocketChannel channel) throws IOException {
        if (Objects.isNull(buffer) || Objects.isNull(channel)) {
            throw new IllegalArgumentException("Error in writing in channel");
        }

        while (buffer.hasRemaining()) {
            channel.write(buffer);
        }
    }
}
