package com.pricing.connection;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pricing.service.QuoteProvider;

@Component
public class QuoteConnectionHandler implements SocketChannelWriter {

    @Autowired
    private QuoteProvider quoteProvider;

    private static final int BUFFER_SIZE = 1024;

    private final int port = 8888;

    Logger logger = Logger.getLogger(QuoteConnectionHandler.class.getSimpleName());

    @PostConstruct
    public void init() {
        logger.info("Starting quote server");
        start();
    }



    private void start() {
        try (Selector selector = Selector.open(); ServerSocketChannel channel = ServerSocketChannel.open()) {
            initChannel(channel, selector);
            boolean keepAlive = true;
            while (keepAlive) {
                if (selector.isOpen()) {
                    final int numKeys = selector.select();
                    if (numKeys > 0) {
                        handleKeys(channel, selector.selectedKeys());
                    }
                } else {
                    keepAlive = false;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to start server.", e);
        }
    }

    private void initChannel(final ServerSocketChannel channel, final Selector selector) throws IOException {

        channel.socket().setReuseAddress(true);
        channel.configureBlocking(false);
        channel.socket().bind(new InetSocketAddress(this.port));
        channel.register(selector, SelectionKey.OP_ACCEPT);
    }

    private void handleKeys(final ServerSocketChannel channel, final Set<SelectionKey> keys) throws IOException {

        final Iterator<SelectionKey> iterator = keys.iterator();
        while (iterator.hasNext()) {

            final SelectionKey key = iterator.next();
            try {
                if (key.isValid()) {
                    if (key.isAcceptable()) {
                        doAccept(channel, key);
                    } else if (key.isReadable()) {
                        readFromChannel(key);
                    } else {
                        throw new UnsupportedOperationException("Key not supported by server.");
                    }
                } else {
                    throw new UnsupportedOperationException("Key not valid.");
                }
            } finally {
                iterator.remove();
            }
        }
    }

    private void doAccept(final ServerSocketChannel channel, final SelectionKey key) throws IOException {
        final SocketChannel client = channel.accept();
        client.configureBlocking(false);
        client.register(key.selector(), SelectionKey.OP_READ);
    }

    private void readFromChannel(final SelectionKey key) throws IOException {

        final SocketChannel client = (SocketChannel) key.channel();
        final ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);

        final int bytesRead = client.read(buffer);
        logger.fine("no of Bytes read: " + bytesRead);

        String incomings[] = new String(buffer.array()).trim().split(" ");
        String out, side;
        int security, qty;
        if(incomings.length==3){
            try {
                security = Integer.parseInt(incomings[0]);
                side = incomings[1];
                qty = Integer.parseInt(incomings[2]);
                double price = quoteProvider.getQuote(security, side, qty);
                out = String.valueOf(price);
            }catch(Exception ec){
                out = "wrong input or error in get data";
                ec.printStackTrace();
                logger.severe("Exception in parsing incoming message: "+ec.getMessage());
            }
        }else{
            out = "wrong input";
        }
        logger.info("Quote to send back is " + out);
        final ByteBuffer responseBuffer = ByteBuffer.wrap(out.trim().getBytes());

        writeIntoChannel(responseBuffer, (SocketChannel) key.channel());
        responseBuffer.clear();
        key.channel().close();
        key.cancel();


    }


    /*@PostConstruct
    public void init(){
        // check some quoted

        Thread thread = new Thread(() -> {
            while(true){

                try {
                    Thread.sleep(3000);
                    quoteService.getQuote(1, "BUY", 100);
                    quoteService.getQuote(2, "BUY", 100);
                    quoteService.getQuote(1, "SELL", 200);
                    quoteService.getQuote(9, "BUY", 600);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

    }*/

}
