package com.pricing.marketmaker;

import com.pricing.service.RefPriceHandler;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

@Component
public class ReferencePriceSourceImpl implements  ReferencePriceSource{

    Logger logger = Logger.getLogger(ReferencePriceSourceImpl.class.getSimpleName());
    ConcurrentHashMap<Integer, Double> lastPriceMap = new ConcurrentHashMap<>();
    /**
     * Subscribe to changes to refernce prices.
     *
     * @param listener callback interface for changes
     */
    @Override
    public void subscribe(ReferencePriceSource.ReferencePriceSourceListener listener){

        RefPriceHandler.getInstance().register(listener);
    }

    /**
     * Returns the last price.
     * return the price or Nan if there is no active subscription
     */
    @Override
    public double get(int securityId){
        return lastPriceMap.keySet().contains(securityId)? lastPriceMap.get(securityId) : Double.NaN;
    }

    /**
     * Callback interface for {@link ReferencePriceSource}
     */
    public class ReferencePriceSourceListenerImpl implements ReferencePriceSource.ReferencePriceSourceListener {

        /**
         * Called when a price has changed.
         *
         * @param securityId security identifier
         * @param price      reference price
         */
        @Override
        public void referencePriceChanged(int securityId, double price){
            lastPriceMap.put(securityId, price);
            logger.fine(String.format("Last price cache updated with security %s with price %s", securityId, price));
        }
    }

    @PostConstruct
    public void init(){
        logger.info( "Subscribing ref price data");
        subscribe(new ReferencePriceSourceListenerImpl());
    }
}
