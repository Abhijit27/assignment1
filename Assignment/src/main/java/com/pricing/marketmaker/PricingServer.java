package com.pricing.marketmaker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.pricing.marketmaker", "com.pricing.connection", "com.pricing.service" })
public class PricingServer {

	public static void main(String[] args) {
		SpringApplication.run(PricingServer.class, args);
		System.out.println("PricingServer Started");
		while (true) {
			System.out.println("Waiting for Quotes Published ... ");
			try {
				Thread.sleep(10000);
			} catch (Exception e) {
				System.out.println("Exception in Server ... ");
			}
		}
	}
}