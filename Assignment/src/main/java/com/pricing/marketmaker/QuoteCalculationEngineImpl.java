package com.pricing.marketmaker;

import org.springframework.stereotype.Component;

@Component
public class QuoteCalculationEngineImpl implements QuoteCalculationEngine{
    /**
     * Computes the price for a product.
     *
     * @param securityId     security identifier
     * @param way            the direction of the trade
     * @param size           the size of the trade
     * @param referencePrice the reference price of the security
    # @return the quote price
     */
    @Override
    public double computeQuote(int securityId, int way, int size, double referencePrice){
        // As of now biz logic of computing quote is not known to me,
        // making some random logic (X*0.5) here to compute
        //TODO apply biz defined logic to compute quote.
        return referencePrice * 0.5;
    }

}
