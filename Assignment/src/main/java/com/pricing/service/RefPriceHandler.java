package com.pricing.service;

import com.pricing.marketmaker.ReferencePriceSource;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class RefPriceHandler {
    Logger logger = Logger.getLogger("RefPriceHandler");
    private RefPriceHandler(){

    }
    static RefPriceHandler refPriceHandler = new RefPriceHandler();
    public static RefPriceHandler getInstance(){
        return refPriceHandler;
    }

    ArrayList<ReferencePriceSource.ReferencePriceSourceListener> subscriptionList = new ArrayList<>();

	public void register(ReferencePriceSource.ReferencePriceSourceListener subscriber){
        subscriptionList.add(subscriber);
    }

    public void generateRefPriceChangeEvent(int security, double price){
        for(ReferencePriceSource.ReferencePriceSourceListener subscriber: subscriptionList){
            subscriber.referencePriceChanged(security, price);
        }
    }

    public void requestPriceInit(ReferencePriceSource.ReferencePriceSourceListener subscriber){
        //TODO there should be a way to initialize prices when a subscription made
        // and price change event can update once price changes
    }

    public void generateSimulatedEvents(){
        Thread refPriceGenSimulator = new Thread(() -> {
            while(true){
                int security = getSecurity1_10();
                double refPrice = getRefPrice100_105();
                generateRefPriceChangeEvent(security, refPrice);
                logger.log(Level.INFO, String.format("Price change event generated for %s, %s", security, refPrice));

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        refPriceGenSimulator.setDaemon(true);
        refPriceGenSimulator.setName("Ref_Price_Simulator");
        refPriceGenSimulator.start();
    }

    int getSecurity1_10(){
        Random rand = new Random();
        int randomNum = rand.nextInt(10)+1;
        return randomNum;
    }

    double getRefPrice100_105(){
        Random r = new Random();
        double randomValue = 100 + (5) * r.nextDouble();
        return randomValue;
    }

    static {
        getInstance().generateSimulatedEvents();
    }

}
