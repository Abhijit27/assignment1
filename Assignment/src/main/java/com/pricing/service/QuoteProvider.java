package com.pricing.service;

import com.pricing.marketmaker.QuoteCalculationEngine;
import com.pricing.marketmaker.ReferencePriceSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class QuoteProvider {

    @Autowired
    private QuoteCalculationEngine quoteCalculationEngine;

    @Autowired
    private ReferencePriceSource referencePriceSource;

    Logger logger = Logger.getLogger(QuoteProvider.class.getSimpleName());

    public double getQuote(int secutiry, String side, int qty){
        Double refPrice = referencePriceSource.get(secutiry);
        if(refPrice.isNaN()){
            logger.severe("could not find ref price for this security: " + secutiry);
            return 0.0;
        }

        double quote = quoteCalculationEngine.computeQuote(secutiry, "BUY".equals(side)? 1: 2, qty, refPrice);
        logger.info("Replying quote as " + quote);
        return quote;
    }
}
