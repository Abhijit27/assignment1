package com.pricing.exception;

public class QuoteNotFoundException extends Exception{
	private static final long serialVersionUID = 1L;
	
    public QuoteNotFoundException(String msg){
        super(msg);
    }
}
